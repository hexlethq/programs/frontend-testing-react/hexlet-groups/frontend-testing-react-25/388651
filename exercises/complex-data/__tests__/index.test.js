const faker = require('faker');

// BEGIN
test('createTransaction has required fields', () => {
    const transaction = faker.helpers.createTransaction();
    // `amount`, `date`, `business`, `name`, `type`, `account`
    expect(transaction).toHaveProperty('amount');
    expect(transaction).toHaveProperty('date');
    expect(transaction).toHaveProperty('business');
    expect(transaction).toHaveProperty('name');
    expect(transaction).toHaveProperty('type');
    expect(transaction).toHaveProperty('account');
});


test('createTransaction creates unique transactions', () => {
    const transaction1 = faker.helpers.createTransaction();
    const transaction2 = faker.helpers.createTransaction();
   expect(transaction1).not.toEqual(transaction2);
})
// END
